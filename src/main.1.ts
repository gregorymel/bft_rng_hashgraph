import {
    Client,
    AccountBalanceQuery,
    Ed25519PrivateKey
} from "@hashgraph/sdk";
import Operator from './key';

async function main() {
    const operatorPrivateKey = Ed25519PrivateKey.fromString(Operator.OPERATOR_KEY);
    const operatorAccount = Operator.OPERATOR_ID;

    console.log(operatorAccount);

    if (operatorPrivateKey == null || operatorAccount == null) {
        throw new Error("environment variables OPERATOR_KEY and OPERATOR_ID must be present");
    }

    const client = Client.forTestnet();

    client.setOperator(operatorAccount, operatorPrivateKey);

    const balance = await new AccountBalanceQuery()
        .setAccountId(operatorAccount)
        .execute(client);

    console.log(`${operatorAccount} balance = ${balance.asTinybar()}`);
}

main();
