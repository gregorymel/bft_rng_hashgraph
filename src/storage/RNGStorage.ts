import {
    IPolyCommitMessageContent,
    IPolyShareMessageContent,
    IPolyEvalMessageContent,
    IEvalCommitMessageContent,
    IImplicateMessageContent
} from "../networkCommunication/Messages";
import { Logger } from "../logger/Logger";

import { toBigIntLE } from "bigint-buffer";

export interface IRNGStorage {
    updateWhiteList(blackList: Set<number>): void;
    getWhiteListedPks(): Array<string>;
    getPk(account: string): string | undefined;
    getWhiteListSize(): number;
    getQuorumSize(): number;
    clearStorage(): void;

    storePolyCommitMessage(sender: string, content: IPolyCommitMessageContent): void;
    countPolyCommitMessages(): number;
    getPolyCommitment(sender: string): Array<Buffer>;

    storeEncSharesMessage(sender: string, content: IPolyShareMessageContent): void;
    countEncSharesMessage(): number;
    getEncShares(sender: string): Array<Buffer>;

    storeEvalCommitMessage(sender: string, content: IEvalCommitMessageContent): void;
    countEvalCommitMessages(): number;
    getEvalCommitMessage(account: string): IEvalCommitMessageContent | undefined;

    storePolyEvalMessage(sender: string, content: IPolyEvalMessageContent): void;
    countPolyEvalMessages(): number;
    getPolyEvalMessage(account: string): IPolyEvalMessageContent | undefined;
    getPolyEvaluations(): Array<[number, bigint]>;

    // storeRevealedKeys(sender: string, content: IRevealKeysMessageContent): void;
    // countRevealKeysMessage(victimAccount: string): number;
    // getRevealedKeys(victimAccount: string): Array<[number, string]> | undefined;
}

export class InMemoryStorage implements IRNGStorage {
    private commitStorage: Array<IPolyCommitMessageContent>;
    private encSharesStorage: Array<IPolyShareMessageContent>;

    private evalCommitStorage: Map<number, IEvalCommitMessageContent> = new Map();
    private evalStorage: Map<number, IPolyEvalMessageContent> = new Map();

    private whitelistMap: Map<string, number>;
    // private whitelistedPks?: Array<string> = undefined;
    private countPolyCommited: number = 0;
    private countEncShares: number = 0;

    constructor(private whitelist: Array<string>, private logger: Logger) {
        const entries = [...whitelist.entries()].map(value => [value[1], value[0]]);
        this.whitelistMap = new Map(entries as Array<[string, number]>);
        this.commitStorage = new Array(this.whitelist.length);
        this.encSharesStorage = new Array(this.whitelist.length);
    }

    getWhiteListedPks(): Array<string> {
        return this.commitStorage.map(message => message.publicKey);
    }

    getPk(account: string): string | undefined {
        const id = this.whitelistMap.get(account);

        if (id !== undefined) {
            return this.commitStorage[id].publicKey;
        } else {
            return undefined;
        }
    }

    getWhiteListSize(): number {
        return this.whitelist.length;
    }

    getQuorumSize(): number {
        return Math.floor(this.whitelist.length / 2) + 1;
    }

    storePolyCommitMessage(sender: string, content: IPolyCommitMessageContent): void {
        const id = this.whitelistMap.get(sender);

        if (id !== undefined) {
            this.commitStorage[id] = content;
            this.countPolyCommited++;

            this.logger.log({
                subject: "Storage",
                storageType: "PolyCommit",
                sender: sender,
                counter: this.countPolyCommited
            });
        }
    }

    countPolyCommitMessages(): number {
        return this.countPolyCommited;
    }

    getPolyCommitment(sender: string): Array<Buffer> {
        const id = this.whitelistMap.get(sender)!;
        // TODO: transpose matrix and return row;
        return this.commitStorage
            .map(message => message.commitment[id]);
    }

    storeEncSharesMessage(sender: string, content: IPolyShareMessageContent): void {
        const id = this.whitelistMap.get(sender);

        if (id !== undefined) {
            this.encSharesStorage[id] = content;
            this.countEncShares++;

            this.logger.log({
                subject: "Storage",
                storageType: "EncShares",
                sender: sender,
                counter: this.countEncShares
            });
        }
    }

    countEncSharesMessage(): number {
        return this.countEncShares;
    }

    getEncShares(sender: string): Array<Buffer> {
        const id = this.whitelistMap.get(sender)!;

        // TODO: transpose matrix and return row;
        return this.encSharesStorage
            .map(encShares => encShares.shares[id]);
    }

    // storePolyEvalMessage(sender: string, content: IPolyEvalMessageContent): void {
    //     const id = this.whitelistMap.get(sender)!;

    //     this.evalStorage[id] = content;
    // }
    storeEvalCommitMessage(sender: string, content: IEvalCommitMessageContent): void {
        const id = this.whitelistMap.get(sender);

        if (id !== undefined) {
            this.evalCommitStorage.set(id, content);

            this.logger.log({
                subject: "Storage",
                storageType: "EvalCommit",
                sender: sender,
                counter: this.evalCommitStorage.size
            });
        }
    }

    countEvalCommitMessages(): number {
        return this.evalCommitStorage.size;
    }

    getEvalCommitMessage(account: string): IEvalCommitMessageContent | undefined {
        const id = this.whitelistMap.get(account);

        if (id !== undefined) {
            return this.evalCommitStorage.get(id);
        }

        return undefined;
    }

    storePolyEvalMessage(sender: string, content: IPolyEvalMessageContent): void {
        const id = this.whitelistMap.get(sender);

        if (id !== undefined) {
            this.evalStorage.set(id, content);

            this.logger.log({
                subject: "Storage",
                storageType: "PolyEval",
                sender: sender,
                counter: this.evalStorage.size
            });
        }
    }

    countPolyEvalMessages(): number {
        return this.evalStorage.size;
    }

    getPolyEvalMessage(account: string): IPolyEvalMessageContent | undefined {
        const id = this.whitelistMap.get(account);

        if (id !== undefined) {
            return this.evalStorage.get(id);
        }

        return undefined;
    }

    getPolyEvaluations(): Array<[number, bigint]> {
        const entries = this.evalStorage.entries();

        // indecies of parties start from 1
        return [...entries].map(entry => [entry[0] + 1, toBigIntLE(entry[1].evaluation)]);
    }

    clearStorage(): void {
        this.commitStorage = new Array();
        this.encSharesStorage = new Array();

        this.evalCommitStorage.clear();
        this.evalStorage.clear();

        this.countPolyCommited = 0;
        this.countEncShares = 0;
    }

    updateWhiteList(blackList: Set<number>): void {
        this.whitelist = this.whitelist.filter((x, i) => {
            if (!blackList.has(i)) {
                return x;
            }
        });

        const entries = [...this.whitelist.entries()].map(value => [value[1], value[0]]);
        this.whitelistMap = new Map(entries as Array<[string, number]>);
    }

    // storeRevealedKeys(sender: string, content: IRevealKeysMessageContent): void {
    //     if (this.revealedKeys === undefined) {
    //         this.revealedKeys = new Map();
    //     }

    //     if (!this.revealedKeys.has(content.victimAccount)) {
    //         this.revealedKeys.set(content.victimAccount, []);
    //     }

    //     const id = this.whitelistMap.get(sender);
    //     if (id !== undefined) {
    //         this.revealedKeys!.get(content.victimAccount)!.push([id, content.privateKey]);
    //     }
    // }

    // countRevealKeysMessage(victimAccount: string): number {
    //     if (this.revealedKeys && this.revealedKeys.has(victimAccount)) {
    //         return this.revealedKeys.get(victimAccount)!.length;
    //     } else {
    //         return 0;
    //     }
    // }

    // getRevealedKeys(victimAccount: string): Array<[number, string]> | undefined {
    //     if (this.revealedKeys) {
    //         return this.revealedKeys.get(victimAccount);
    //     }

    //     return undefined;
    // }
}
