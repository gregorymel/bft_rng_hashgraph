import { AccountCreateTransaction, Client, Ed25519PrivateKey } from "@hashgraph/sdk";
import { writeFileSync } from "fs";

import Operator from '../key';

async function main() {
    const operatorPrivateKey = Ed25519PrivateKey.fromString(Operator.OPERATOR_KEY);
    const operatorAccount = Operator.OPERATOR_ID;

    if (operatorPrivateKey == null || operatorAccount == null) {
        throw new Error("environment variables OPERATOR_KEY and OPERATOR_ID must be present");
    }

    const client = Client.forTestnet();
    client.setOperator(operatorAccount, operatorPrivateKey);

    const privateKey = await Ed25519PrivateKey.generate();

    console.log("Automatic signing example");
    console.log(`private = ${privateKey.toString()}`);
    console.log(`public = ${privateKey.publicKey.toString()}`);

    const transactionId = await new AccountCreateTransaction()
        .setKey(privateKey.publicKey)
        .setInitialBalance(1000000000)
       // .setGenerateRecord Available in the JS SDK but not in the JAVA SDK
        .execute(client);

    const transactionReceipt = await transactionId.getReceipt(client);
    const newAccountId = transactionReceipt.getAccountId();

    console.log(`account = ${newAccountId}`);

    writeFileSync(`./config/${newAccountId}.key`, privateKey.toString(), "utf-8");
}

const promises = [...Array(Number(process.env.PARTIES))].map(() => main());
Promise.all(promises)
    .then(() => {
        process.exit(0);
    });

// main()
//     .then(() => {
//         process.exit(0);
//     });
