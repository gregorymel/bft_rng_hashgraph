import { IConfig } from "./Config";
import { NetworkMessagesFilter } from "./networkCommunication/NetworkMessageFilter";
import { BftRNGTerm } from "./RNGTerm";
import { Block } from "./Block";

export type onGeneratedValueCB = (value: bigint) => void;

export class BftRNG {
    private readonly onCommittedListeners: onGeneratedValueCB[];
    private rngTerm?: BftRNGTerm;
    private networkMessagesFilter: NetworkMessagesFilter;

    constructor(private readonly config: IConfig) {
        this.onCommittedListeners = [];

        this.networkMessagesFilter = new NetworkMessagesFilter(config.networkCommunication, config.topicId);
    }

    public registerOnCommitted(bc: onGeneratedValueCB): void {
        this.onCommittedListeners.push(bc);
    }

    // public start(fieldValue: bigint): void {
    //     this.disposeRNGTerm();
    //     this.createRNGTerm(fieldValue);
    // }

    public startRNGTerm(): void {
        if (this.rngTerm) {
            this.rngTerm.startTerm();
            this.networkMessagesFilter.setMessageHandler(this.rngTerm);
        }
    }

    public dispose(): void {
        this.onCommittedListeners.length = 0;
        this.disposeRNGTerm();
    }

    private notifyCommitted(value: bigint): void {
        this.onCommittedListeners.map(cb => cb(value));
    }

    private disposeRNGTerm(): void {
        if (this.rngTerm) {
            this.rngTerm.dispose();
            this.rngTerm = undefined;
        }
    }

    public createRNGTerm(fieldValue: bigint): void {
        this.disposeRNGTerm();

        // const messagesFilter = new NetworkMessagesFilter(this.config.networkCommunication, this.config.topicId);
        this.rngTerm = new BftRNGTerm(this.config, fieldValue, block => {
            this.notifyCommitted(block);
            this.disposeRNGTerm();
            this.networkMessagesFilter;
        });
        // this.rngTerm.startTerm();
    }
}
