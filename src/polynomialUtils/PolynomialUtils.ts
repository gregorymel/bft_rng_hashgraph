import * as secp256k1 from "noble-secp256k1";
import { randBetween, modPow, toZn, modInv  } from "bigint-crypto-utils";
import { toBufferLE, toBigIntLE } from "bigint-buffer";

import { IKeyManager } from "../keyManager/KeyManager";

const DEAFAULT_BIT_SIZE = 128;

export interface IPolyUtils {
    modulusLength: number;
    generatePolynomial(degree: number, fieldValue: bigint): Polynomial;
    commitPolynomial(poly: Polynomial, partiesNum: number): Array<Buffer>;
    createEncShares(poly: Polynomial, pks: Array<string>): Array<Buffer> | undefined;
    commitEvaluation(value: bigint): Buffer;
    verifyEvalCommitment(commitmentEval: Buffer, commitmentParts: Array<Buffer>): boolean;
    verifyEval(evaluation: bigint, commitmentEval: Buffer): boolean;
    assembleValue(encShares: Array<Buffer>): bigint | undefined;
    interpolatePolynomial(evals: Array<[number, bigint]>, modulus: bigint): bigint;
}

export interface PolyUtilsCtor {
    new(keyManager: IKeyManager, modulusLength: number): IPolyUtils;
}

export class Polynomial {
    constructor(private coeffs: Array<bigint>, private p: bigint) {
    }

    static fromRandom(degree: number, p: bigint): Polynomial {
        const coeffs = Array.from({length: degree + 1}, () => randBetween(p - 1n)); // [1, p]

        return new Polynomial(coeffs, p);
    }

    public at(x: bigint | number): bigint {
        const result = this.coeffs
            .map((value, index) => toZn(value * modPow(x, index, this.p), this.p))
            .reduce((acc, curValue) => toZn(acc + curValue, this.p));

        return result;
    }

    public add(other: Polynomial): Polynomial | undefined {
        if (other.filedValue != this.p) {
            return undefined;
        }

        const coeffs = other.polyCoeffs
            .map((value, i) => toZn(value + this.coeffs[i], this.p));

        return new Polynomial(coeffs, this.p);
    }

    get polyCoeffs(): Array<bigint> {
        return this.coeffs;
    }

    get filedValue(): bigint {
        return this.p;
    }

    get polyDegree(): number {
        return this.coeffs.length - 1;
    }
}

export class PolyUtilsP256 implements IPolyUtils {
    constructor(private keyManager: IKeyManager, public modulusLength: number = DEAFAULT_BIT_SIZE) {}

    generatePolynomial(degree: number, fieldValue: bigint): Polynomial {
        return Polynomial.fromRandom(degree, fieldValue);
    }

    commitPolynomial(poly: Polynomial, partiesNum: number): Array<Buffer> {
        const base_point = secp256k1.BASE_POINT;
        return [...Array(partiesNum).keys()]
            .map((i) => base_point.multiply(poly.at(i + 1)).toRawBytes(true))
            .map(arr => Buffer.from(arr.buffer));
    }

    createEncShares(poly: Polynomial, pks: Array<string>): Array<Buffer> | undefined {
        if (!this.keyManager) {
            return undefined;
        }

        const encShares = pks
            .map((pk, index) => {
                return this.keyManager!.encrypt(toBufferLE(poly.at(index + 1), this.modulusLength / 8), pk);
            });

        return encShares;
    }

    assembleValue(encShares: Array<Buffer>): bigint | undefined {
        if (!this.keyManager) {
            return undefined;
        }

        const values = encShares.map(encValue => this.keyManager!.decrypt(encValue));

        return values
            .map(buf => toBigIntLE(buf))
            .reduce((acc, value) => acc + value);
    }

    verifyEvalCommitment(commitmentEval: Buffer, commitmentParts: Array<Buffer>): boolean {
        const point = commitmentParts
            .map(c => secp256k1.Point.fromHex(c))
            .reduce((acc, curValue) => acc.add(curValue));

        const evalPoint = secp256k1.Point.fromHex(commitmentEval);

        return (point.x === evalPoint.x) && (point.y === evalPoint.y);
    }

    verifyEval(evaluation: bigint, commitmentEval: Buffer): boolean {
        const expectedPoint = secp256k1.BASE_POINT.multiply(evaluation);
        const actualPoint = secp256k1.Point.fromHex(commitmentEval);

        return (expectedPoint.x === actualPoint.x) && (expectedPoint.y === actualPoint.y);
    }

    commitEvaluation(value: bigint): Buffer {
        const valuePoint = secp256k1.BASE_POINT
            .multiply(value)
            .toRawBytes(true);

        return Buffer.from(valuePoint.buffer);
    }

    interpolatePolynomial(evals: Array<[number, bigint]>, modulus: bigint): bigint {
        const points = evals.map(entry => [toZn(entry[0], modulus), toZn(entry[1], modulus)]);
        // tslint:disable-next-line: prefer-const
        let result: bigint = 0n;
        let numi: bigint;
        let deni: bigint;
        for (const [i, [xi, yi]] of points.entries()) {
            numi = 1n;
            deni = 1n;
            for (const [j, [xj, yj]] of points.entries()) {
                if (i !== j) {
                    numi *= xj;
                    deni *= (xj - xi);
                }
            }
            deni = toZn(deni + modulus, modulus);
            numi = toZn(numi, modulus);
            result = toZn(result + yi * numi * modInv(deni, modulus), modulus);
        }

        return result;
    }
}
