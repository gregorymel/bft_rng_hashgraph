import { PolyUtilsCtor } from "./polynomialUtils/PolynomialUtils";
import { Trigger } from "./roundTrigger/RoundTrigger";
import { KeyManagerFactory } from "./keyManager/KeyManager";
import { Logger } from "./logger/Logger";
import { INetworkCommunication } from "./networkCommunication/NetworkCommunication";
import { IRNGStorage } from "./storage/RNGStorage";

export interface IConfig {
    topicId: string;
    networkCommunication: INetworkCommunication;
    polyUtilsCtor: PolyUtilsCtor;
    keyManagerFactory: KeyManagerFactory;
    logger: Logger;
    roundTrigger: Trigger;
    implicateTrigger: Trigger;
    rngStorage: IRNGStorage;
}
