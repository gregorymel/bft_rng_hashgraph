import * as crypto from "crypto";

export interface IKeyManager {
    encrypt(data: Buffer, publicKey: string): Buffer;
    decrypt(ciphertext: Buffer): Buffer;

    getMyPublicKey(): string;
    getMyPrivateKey(): string;
}

export interface KeyManagerCtor {
    new(privateKey: string, publicKey: string): IKeyManager;
}

export class KeyManagerFactory {
    constructor(private ctor: KeyManagerCtor) {}

    private generateNewKeys(modulusLength: number): [string, string] {
        const privKeyEncoding = {
            type: "pkcs1",
            format: "pem"
        };

        const pubKeyEncoding = {
            type: "pkcs1",
            format: "pem",
        };

        const options = {
            modulusLength: modulusLength,
            publicKeyEncoding: pubKeyEncoding,
            privateKeyEncoding: privKeyEncoding
        };

        const keyPair = crypto.generateKeyPairSync("rsa", (options as crypto.RSAKeyPairOptions<"pem", "pem">));

        return [keyPair.privateKey, keyPair.publicKey];
    }

    createNewKeyManager(modulusLength: number): IKeyManager {
        const [privateKey, publicKey] = this.generateNewKeys(modulusLength);

        return new this.ctor(privateKey, publicKey);
    }

    createWithKeyPair(privateKey: string, publicKey: string): IKeyManager {
        return new this.ctor(privateKey, publicKey);
    }
}

export class KeyManagerRSA implements IKeyManager {
    constructor(private privateKey: string, private publicKey: string) {}

    public encrypt(data: Buffer, publicKey: string): Buffer {
        const ciphertext = crypto.publicEncrypt(publicKey, data);
        return ciphertext;
    }

    public decrypt(ciphertext: Buffer): Buffer {
        const data = crypto.privateDecrypt(this.privateKey!, ciphertext);
        return data;
    }

    public getMyPublicKey(): string {
        return this.publicKey;
    }

    public getMyPrivateKey(): string {
        return this.privateKey;
    }
}
