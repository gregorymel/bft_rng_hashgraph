import debug from "debug";

// // FLOW
// type FlowPolyCommit = {
//     FlowType: "PolyCommit",
//     blockHeight: number,
//     view: number
// };

// type FlowPolyShare = {
//     FlowType: "Commit",
//     blockHeight: number,
//     view: number,
//     blockHash: string
// };

// type Flow = {
//     FlowType: "LeaderChange",
//     blockHeight: number,
//     newView: number,
//     leaderPk: string
// };

/// Storage
type StorageLogData = {
    subject: "Storage",
    storageType: string,
    sender: string,
    counter: number
};

/// Consensus Service
type ConsensusServiceSendLogData = {
    subject: "ConsensusService",
    message: string
};

// WARNING
type InfoLogData = { subject: "Info", message: string, metaData?: object };

// WARNING
type WarningLogData = { subject: "Warning", message: string, metaData?: object };

export type LogTypes = InfoLogData | ConsensusServiceSendLogData | StorageLogData | WarningLogData;

export interface Logger {
    log(data: LogTypes): void;
}

export class ConsoleLogger implements Logger {
    private readonly debug_storage: debug.Debugger;
    private readonly debug_info: debug.Debugger;
    private readonly debug_cs: debug.Debugger;
    private readonly debug_warning: debug.Debugger;

    constructor(private id: string) {
        this.debug_storage = debug.debug(`[${id}]:Storage`);
        this.debug_info = debug.debug(`[${id}]:Info`);
        this.debug_cs = debug.debug(`[${id}]:ConsensusService`);
        this.debug_warning = debug.debug(`[${id}]:Warning`);
    }

    public log(data: LogTypes): void {
        // const dataStr = JSON.stringify(data, undefined, 2);
        const dataStr = data;
        switch (data.subject) {
            case "Storage":
                this.debug_storage(dataStr);
                break;
            case "ConsensusService":
                this.debug_cs(dataStr);
                break;
            case "Info":
                this.debug_info(dataStr);
                break;
            case "Warning":
                this.debug_warning(dataStr);
                break;
        }
    }
}

export class SilentLogger implements Logger {
    constructor(private id: string) {}

    public log(data: LogTypes): void {}
}
