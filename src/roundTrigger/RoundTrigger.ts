export interface Trigger {
    registerOnTrigger(cb: () => void): void;
    unregisterOnTrigger(): void;
}

export class TimerBasedTrigger implements Trigger {
    private cb?: () => void;
    private timer?: NodeJS.Timer;

    constructor(private minTimeout: number) {
    }

    public registerOnTrigger(cb: () => void): void {
        this.cb = cb;
        this.timer = setTimeout(() => this.onTimeout(), this.minTimeout);
    }

    public unregisterOnTrigger(): void {
        this.cb = undefined;
        this.stop();
    }

    private stop(): void {
        if (this.timer) {
            clearTimeout(this.timer);
            this.timer = undefined;
        }
    }

    private onTimeout(): void {
        if (this.cb) {
            this.cb();
        }
    }
}
