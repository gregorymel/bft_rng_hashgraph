import {
    Client,
    ConsensusMessageSubmitTransaction,
    // ConsensusSubmitMessageTransaction,
    ConsensusTopicCreateTransaction,
    Ed25519PrivateKey,
    Ed25519PublicKey,
    MirrorClient,
    MirrorConsensusTopicQuery
} from "@hashgraph/sdk";
// require("dotenv").config();
import Operator from './key'

async function main() {
    const operatorPrivateKey = Ed25519PrivateKey.fromString(Operator.OPERATOR_KEY);
    const operatorAccount = Operator.OPERATOR_ID;
    const mirrorNodeAddress = "hcs.testnet.mirrornode.hedera.com:5600";

    if (operatorPrivateKey == null || operatorAccount == null) {
        throw new Error("environment variables OPERATOR_KEY and OPERATOR_ID must be present");
    }

    const mirrorClient = new MirrorClient(mirrorNodeAddress);

    const client = Client.forTestnet();
    client.setOperator(operatorAccount, operatorPrivateKey);

    // const submitKey = await Ed25519PrivateKey.generate();
    // const submitPublicKey = submitKey.publicKey;

    const transactionId = await new ConsensusTopicCreateTransaction()
        .setTopicMemo("HCS topic with submit key")
        // .setSubmitKey(operatorPrivateKey.publicKey)
        .execute(client);

    const receipt = await transactionId.getReceipt(client);
    const topicId = receipt.getConsensusTopicId();

    console.log(`Created new topic ${topicId}`);

    new MirrorConsensusTopicQuery()
        .setTopicId(topicId)
        .subscribe(
            mirrorClient,
            (message) => console.log(message.toString()),
            (error) => console.log(`Error: ${error}`)
        );

    // //Submits a message to a public topic 
    // const txId = await new ConsensusMessageSubmitTransaction()
    //     .setTopicId(topicId)
    //     .setMessage("hello, HCS! ")
    //     .build(client)
    //     .sign(submitKey)
    //     .execute(client);

    // let rec = txId.getReceipt(client);

    for (let i = 0; ; i += 1) {
        await (await new ConsensusMessageSubmitTransaction()
            .setTopicId(topicId)
            .setMessage(`Hello, HCS! Message ${i}`)
            .build(client)
            // .sign(operatorPrivateKey) // Must sign by the topic's submitKey
            .execute(client))
            .getReceipt(client);

        console.log(`Sent message ${i}`);

        await sleep(2500);
    }
}

function sleep(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}

main();
