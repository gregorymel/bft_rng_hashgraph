import {
    MessageType,
    IPolyCommitMessageContent,
    IPolyShareMessageContent,
    IPolyEvalMessageContent,
    IEvalCommitMessageContent,
    IImplicateMessageContent,
} from "./Messages";

export interface MessagesHandler {
    getView(): number;
    onReceivePolyCommit(message: IPolyCommitMessageContent, sender: string): any;
    onReceivePolyShare(message: IPolyShareMessageContent, sender: string): any;
    onReceiveEvalCommit(message: IEvalCommitMessageContent, sender: string): any;
    onReceivePolyEval(message: IPolyEvalMessageContent, sender: string): any;
    onReceiveImplicate(message: IImplicateMessageContent, sender: string): any;
}
