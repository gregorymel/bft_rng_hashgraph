// import { KeyManager, Block } from "..";
import {
    MessageType,
    IPolyCommitMessageContent,
    IPolyShareMessageContent,
    IPolyEvalMessageContent,
    IEvalCommitMessageContent,
    IImplicateMessageContent
} from "./Messages";
import { IPolyUtils, Polynomial } from "../polynomialUtils/PolynomialUtils";
import { toBufferLE } from "bigint-buffer";

export class MessagesFactory {
    constructor(private polyUtils: IPolyUtils) { }

    createPolyCommitMessage(poly: Polynomial, partiesNum: number, pk: string): IPolyCommitMessageContent {
        const commitment = this.polyUtils.commitPolynomial(poly, partiesNum);

        return {
            publicKey: pk,
            commitment: commitment
        };
    }

    createPolyShareMessage(poly: Polynomial, pks: Array<string>): IPolyShareMessageContent {
        const shares = this.polyUtils.createEncShares(poly, pks)!;

        return {
            shares: shares
        };
    }

    createEvalCommitMessage(commitEval: Buffer): IEvalCommitMessageContent {
        return {
            commitment: commitEval
        };
    }

    createPolyEvalMessage(evaluation: bigint): IPolyEvalMessageContent {
        const byteLength = Math.ceil(evaluation.toString(2).length / 8);
        return {
            evaluation: toBufferLE(evaluation, byteLength)
        };
    }

    createImplicateMessage(privateKey: string): IImplicateMessageContent {
        return {
            privateKey: privateKey
        };
    }
}
