import {
    MessageType,
    IProtocolRawMessage,
    IPolyCommitMessageContent,
    IPolyShareMessageContent,
    IEvalCommitMessageContent,
    IPolyEvalMessageContent,
    deserializeMessageContent,
    IImplicateMessageContent
} from "./Messages";
import { INetworkCommunication } from "./NetworkCommunication";
import { MessagesHandler } from "./MessagesHandler";

export class NetworkMessagesFilter {
    private messagesHandler?: MessagesHandler = undefined;
    private messagesCache: [string, any][] = [];
    private view: number = 0;

    constructor(private readonly networkCommunication: INetworkCommunication, topicId: string) {
        this.networkCommunication.registerOnMessage(topicId, (consensusRawMessage: IProtocolRawMessage, sender: string) => {
            this.onConsensusMessage(consensusRawMessage, sender);
        });
    }

    private onConsensusMessage(consensusRawMessage: IProtocolRawMessage, sender: string): void {
        if (this.messagesHandler && this.view < this.messagesHandler.getView()) {
            this.view = this.messagesHandler.getView();
            this.consumeCacheMessages();
        }

        const content = deserializeMessageContent(consensusRawMessage.content);

        // console.log(consensusRawMessage);

        const myAccount = this.networkCommunication.getOperatorAccount();
        if (sender === myAccount) {
            return;
        }

        // if (this.networkCommunication.isMember(senderPk) === false) {
        //     return;
        // }

        // if (content.signedHeader.blockHeight < this.blockHeight) {
        //     return;
        // }

        const message = {
            messageType: consensusRawMessage.messageType,
            view: consensusRawMessage.view,
            content: content,
        };

        if (this.messagesHandler === undefined || this.view < message.view) {
            this.messagesCache.push([sender, message]);
            return;
        }

        this.processConsensusMessage(message, sender);
    }

    private processConsensusMessage(message: any, sender: string): void {
        switch (message.messageType) {
            case MessageType.POLY_COMMIT: {
                this.messagesHandler!.onReceivePolyCommit(message.content as IPolyCommitMessageContent, sender);
                break;
            }
            case MessageType.POLY_SHARE: {
                this.messagesHandler!.onReceivePolyShare(message.content as IPolyShareMessageContent, sender);
                break;
            }
            case MessageType.EVAL_COMMIT: {
                this.messagesHandler!.onReceiveEvalCommit(message.content as IEvalCommitMessageContent, sender);
                break;
            }
            case MessageType.POLY_EVAL: {
                this.messagesHandler!.onReceivePolyEval(message.content as IPolyEvalMessageContent, sender);
                break;
            }
            case MessageType.IMPLICATE: {
                this.messagesHandler!.onReceiveImplicate(message.content as IImplicateMessageContent, sender);
                break;
            }
        }
    }

    public setMessageHandler(messagesHandler: MessagesHandler) {
        this.messagesHandler = messagesHandler;
        this.view = messagesHandler.getView();
        this.consumeCacheMessages();
    }

    // private consumeCacheMessages(): void {
    //     for (const [sender, message] of this.messagesCache) {
    //         this.processConsensusMessage(message, sender);
    //     }
    // }

    private consumeCacheMessages(): void {
        this.messagesCache = this.messagesCache.reduce((prev, current) => {
            if (current[1].view === this.view) {
                this.processConsensusMessage(current[1], current[0]);
            } else {
                prev.push(current);
            }
            return prev;
        }, [] as [string, any][]);
    }
}