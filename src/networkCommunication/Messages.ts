import BJSON from "buffer-json";

export type IMessageContent = IPolyCommitMessageContent
                            | IPolyShareMessageContent
                            | IPolyEvalMessageContent
                            | IEvalCommitMessageContent
                            | IImplicateMessageContent;

export function serializeMessageContent(content: IMessageContent): string {
    return BJSON.stringify(content);
}

export function deserializeMessageContent(content: string): IMessageContent {
    return BJSON.parse(content);
}

export enum MessageType {
    INIT = 0,
    READY = 1,
    POLY_COMMIT = 2,
    POLY_SHARE = 3,
    EVAL_COMMIT = 4,
    POLY_EVAL = 5,
    DROP_PARTY = 6,
    IMPLICATE = 7,
    REVEAL_KEYS = 8
}

export interface IProtocolRawMessage {
    messageType: MessageType;
    view: number;
    content: string;
}

export interface IInitMessageContent {
    roomId: string;
}

export interface IReadyMessageContent {
}

export interface IPolyCommitMessageContent {
    publicKey: string;
    commitment: Array<Buffer>;
}

export interface IPolyShareMessageContent {
    shares: Array<Buffer>;
}

export interface IEvalCommitMessageContent {
    commitment: Buffer;
}

export interface IPolyEvalMessageContent {
    evaluation: Buffer;
}

export interface IImplicateMessageContent {
    privateKey: string;
}