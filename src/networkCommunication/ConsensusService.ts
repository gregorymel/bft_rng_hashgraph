import {
    Client,
    ConsensusMessageSubmitTransaction,
    ConsensusTopicCreateTransaction,
    ConsensusTopicId,
    Ed25519PrivateKey,
    Ed25519PublicKey,
    MirrorClient,
    MirrorConsensusTopicQuery
} from "@hashgraph/sdk";

import {
    INetworkCommunication,
    NetworkCommunicationCallback,
} from "./NetworkCommunication";

import {
    IProtocolRawMessage
} from "./Messages";

import { v4 } from "uuid";

import { Mutex } from "async-mutex";

const MESSAGE_SIZE = 2048;

interface IChunk {
    uid: string; // Unique id of current stream
    seq: number; // Sequence number
    total: number;
    operatorId: string; // TODO: replace on signature
    body: string; // chunk of data
}

class OutputTxStream {
    constructor(private operatorAccount: string, private client: Client) {
    }

    private splitMessage(message: string): Array<IChunk> {
        const chunks: Array<IChunk> = [];
        const uid = v4();
        const total = Math.ceil(message.length / MESSAGE_SIZE);
        for (let seq = 0; seq < total; seq++) {
            chunks.push({
                uid: uid,
                seq: seq,
                total: total,
                operatorId: this.operatorAccount,
                body: message.slice(seq * MESSAGE_SIZE, (seq + 1) * MESSAGE_SIZE)
            });
        }

        return chunks;
    }

    private async writeChunk(topicId: string, chunk: IChunk) {
        const chunkStr = JSON.stringify(chunk);

        const transactionId  = await new ConsensusMessageSubmitTransaction()
            .setTopicId(topicId)
            .setMessage(chunkStr)
            .build(this.client)
            .execute(this.client);

        const receipt = await transactionId.getReceipt(this.client);
        // console.log(receipt.status);
    }

    public async write(topicId: string, rawMessage: IProtocolRawMessage): Promise<void> {
        const messageStr = JSON.stringify({
            ...rawMessage
        });

        const promises = this.splitMessage(messageStr)
            .map(chunk => this.writeChunk(topicId, chunk));

        await Promise.all(promises);
    }
}

class InputTxSream {
    private mutex: Mutex;
    private chunkMap: Map<string, string[]> = new Map();

    constructor(private mirrorClient: MirrorClient) {
        this.mutex = new Mutex();
    }

    private handleChunk(chunkStr: string, cb: NetworkCommunicationCallback): void {
        const chunk = JSON.parse(chunkStr) as IChunk;

        if (chunk.total === 1) {
            cb(JSON.parse(chunk.body), chunk.operatorId);
            return;
        }

        const chunkKey = chunk.uid + chunk.operatorId;
        let stream = this.chunkMap.get(chunkKey);
        if (stream === undefined) {
            stream = new Array(chunk.total);
            stream[chunk.seq] = chunk.body;
            this.chunkMap.set(chunkKey, stream);
            return;
        } else {
            stream[chunk.seq] = chunk.body;
        }

        const nonEmpty = stream.filter(str => str !== undefined).length;
        if (nonEmpty === chunk.total) {
            const message = JSON.parse("".concat(...stream));
            console.error("[InputTxStream]", message.messageType, chunk.operatorId);
            this.chunkMap.delete(chunkKey);
            cb(message, chunk.operatorId);
            return;
        }

        return;
    }

    public read(topicId: string, cb: NetworkCommunicationCallback): void {
        const subscriptionToken = ConsensusTopicId.fromString(topicId);
        new MirrorConsensusTopicQuery()
            .setTopicId(subscriptionToken)
            .subscribe(
                this.mirrorClient,
                (chunkRaw) => {
                    const chunkStr = chunkRaw.toString();
                    this.mutex.runExclusive(() => {
                        this.handleChunk(chunkStr, cb);
                    });
                },
                // TODO add error handler
            );
    }
}

export class HederaConsensusService implements INetworkCommunication {
    private client: Client;
    private mirrorClient: MirrorClient;
    private inputStream: InputTxSream;
    private outputStream: OutputTxStream;

    constructor(private operatorPrivateKey: Ed25519PrivateKey, private operatorAccount: string, mirrorNodeAddress: string) {
        this.mirrorClient = new MirrorClient(mirrorNodeAddress);
        this.client = Client.forTestnet();
        this.client.setOperator(operatorAccount, operatorPrivateKey);

        this.inputStream = new InputTxSream(this.mirrorClient);
        this.outputStream = new OutputTxStream(this.operatorAccount, this.client);
    }

    public async createTopic(): Promise<string> {
        const transactionId = await new ConsensusTopicCreateTransaction()
            .setTopicMemo("BFT RNG")
            .execute(this.client);

        const receipt = await transactionId.getReceipt(this.client);
        const subscriptionToken = receipt.getConsensusTopicId();

        return subscriptionToken.toString();
    }

    public getOperatorAccount(): string {
        return this.operatorAccount;
    }

    public async sendMessage(topicId: string, rawMessage: IProtocolRawMessage): Promise<void> {
        await this.outputStream.write(topicId, rawMessage);
    }

    public registerOnMessage(topicId: string, cb: NetworkCommunicationCallback): void {
        this.inputStream.read(topicId, cb);
    }

    public unRegisterOnMessage(topicId: string): void {
        // const subscriptionToken = ConsensusTopicId.fromString(topicId);
    }
}
