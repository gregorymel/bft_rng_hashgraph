import { MessageType, IProtocolRawMessage } from "./Messages";

export type NetworkCommunicationCallback = (consensusRawMessage: IProtocolRawMessage, sender: string) => void;

export interface INetworkCommunication {
    createTopic(): Promise<string> | string;
    sendMessage(topicId: string, rawMessage: IProtocolRawMessage): Promise<void>;
    registerOnMessage(topicId: string, cb: NetworkCommunicationCallback): void;
    unRegisterOnMessage(topicId: string): void;
    getOperatorAccount(): string;
}
