import { IConfig } from "./Config";
import { INetworkCommunication } from "./networkCommunication/NetworkCommunication";
import { MessagesFactory } from "./networkCommunication/MessageFactory";
import { IPolyUtils, Polynomial, PolyUtilsCtor } from "./polynomialUtils/PolynomialUtils";
import { IKeyManager, KeyManagerFactory } from "./keyManager/KeyManager";
import { IRNGStorage, InMemoryStorage } from "./storage/RNGStorage";
import { MessagesHandler } from "./networkCommunication/MessagesHandler";
import { Logger } from "./logger/Logger";
import { Trigger } from "./roundTrigger/RoundTrigger";
import { NetworkMessagesFilter } from "./networkCommunication/NetworkMessageFilter";
import {
    MessageType,
    IProtocolRawMessage,
    IPolyCommitMessageContent,
    IPolyShareMessageContent,
    IPolyEvalMessageContent,
    IEvalCommitMessageContent,
    serializeMessageContent,
    IImplicateMessageContent
} from "./networkCommunication/Messages";

// TODO: only for test, remove!
import { toBigIntLE } from "bigint-buffer";

const RSA_KEY_SIZE = 2048;
const MODULUS_BIT_LENGTH = 128;

export class BftRNGTerm implements MessagesHandler {
    private readonly roundTrigger: Trigger;
    private readonly implicateTrigger: Trigger;
    private readonly networkCommunication: INetworkCommunication;
    private readonly polyUtilsCtor: PolyUtilsCtor;
    private readonly rngStorage: IRNGStorage;
    private readonly keyManagerFactory: KeyManagerFactory;
    private readonly logger: Logger;

    private fieldValue: bigint;
    private keyManager?: IKeyManager;
    private polyUtils?: IPolyUtils;
    private poly?: Polynomial;
    private messagesFactory?: MessagesFactory;
    // private networkMessageFilter: NetworkMessagesFilter;

    private view: number = 0;
    private myAccount: string;
    private topicId: string;

    private blackList: Set<number> = new Set();

    constructor(
        config: IConfig,
        // networkMessageFilter: NetworkMessagesFilter,
        fieldValue: bigint,
        private onGeneratedNumber: (num: bigint) => void
    ) {
        // config
        this.keyManagerFactory = config.keyManagerFactory;
        this.networkCommunication = config.networkCommunication;
        this.rngStorage = config.rngStorage;
        this.fieldValue = fieldValue;
        this.logger = config.logger;
        this.roundTrigger = config.roundTrigger;
        this.implicateTrigger = config.implicateTrigger;
        this.polyUtilsCtor = config.polyUtilsCtor;
        // this.networkMessageFilter = networkMessageFilter;

        this.myAccount = this.networkCommunication.getOperatorAccount();
        this.topicId = config.topicId;
    }

    public startTerm(): void {
        this.initView(0);
    }

    public getView(): number {
        return this.view;
    }

    public dispose(): void {
        this.roundTrigger.unregisterOnTrigger();
    }

    private setView(view: number): void {
        this.initView(view);
    }

    private initView(view: number): void {
        this.view = view;
        this.roundTrigger.registerOnTrigger(() => this.moveToNextRound());

        const keyManager = this.keyManagerFactory.createNewKeyManager(RSA_KEY_SIZE);
        const myPk = keyManager.getMyPublicKey()!;
        // TODO: handle quorumSize equal to 1
        const degree = this.rngStorage.getQuorumSize() - 1;

        this.polyUtils = new this.polyUtilsCtor(keyManager, MODULUS_BIT_LENGTH);
        this.messagesFactory = new MessagesFactory(this.polyUtils);
        this.poly = this.polyUtils.generatePolynomial(degree, this.fieldValue);
        this.keyManager = keyManager;


        const partiesNum = this.rngStorage.getWhiteListSize();
        const content = this.messagesFactory.createPolyCommitMessage(this.poly!, partiesNum, myPk);
        this.rngStorage.storePolyCommitMessage(this.myAccount, content);
        this.sendPolyCommit(content);

        this.logger.log({
            subject: "Info",
            message: "Term started"
        });
    }

    private moveToNextRound(): void {
        this.logger.log({subject: "Warning", message: `move to the next round...`});

        if (this.blackList.size !== 0) {
            this.rngStorage.updateWhiteList(this.blackList);

            this.rngStorage.clearStorage();
            this.blackList.clear();

            this.initView(this.view + 1);
        }
    }

    private sendPolyCommit(content: IPolyCommitMessageContent): void {
        const consesnsusRawMessage: IProtocolRawMessage = {
            messageType: MessageType.POLY_COMMIT,
            view: this.view,
            content: serializeMessageContent(content),
        };
        this.networkCommunication.sendMessage(this.topicId, consesnsusRawMessage)
            .then(() => {
                this.logger.log({
                    subject: "ConsensusService",
                    message: "poly_commit"
                });
            });
        // const { blockHeight, view, blockHash } = message.content.signedHeader;
        // this.logger.log({
        //     subject: "ConsensusService",
        //     message: "poly_commit"
        // });
    }

    public onReceivePolyCommit(content: IPolyCommitMessageContent, sender: string): void {
        this.rngStorage.storePolyCommitMessage(sender, content);

        const countPolyCommited = this.rngStorage.countPolyCommitMessages();
        this.logger.log({subject: "Info", message: `countPolyCommited ${countPolyCommited}`});
        if (countPolyCommited === this.rngStorage.getWhiteListSize()) {
            const targetPks = this.rngStorage.getWhiteListedPks();
            const content = this.messagesFactory!.createPolyShareMessage(this.poly!, targetPks);

            this.sendPolyShare(content);

            this.rngStorage.storeEncSharesMessage(this.myAccount, content);
            this.checkPolyShared(this.myAccount);
        }
    }

    private sendPolyShare(content: IPolyShareMessageContent): void {
        const consesnsusRawMessage: IProtocolRawMessage = {
            messageType: MessageType.POLY_SHARE,
            view: this.view,
            content: serializeMessageContent(content)
        };
        this.networkCommunication.sendMessage(this.topicId, consesnsusRawMessage);
        this.logger.log({
            subject: "ConsensusService",
            message: "poly_share"
        });
    }

    private validateEvalCommit(commitmentEval: Buffer, sender: string): boolean {
        const commitmentPoly = this.rngStorage.getPolyCommitment(sender);

        return this.polyUtils!.verifyEvalCommitment(commitmentEval, commitmentPoly);
    }

    public onReceivePolyShare(message: IPolyShareMessageContent, sender: string): void {
        this.rngStorage.storeEncSharesMessage(sender, message);
        this.checkPolyShared(sender);
    }

    // private validatePolyShare(account: string): boolean {
    //     const encShares = this.rngStorage.getEncShares(account);
    //     const accValue = this.polyUtils.assembleValue(encShares);

    //     if (accValue == undefined) {
    //         return false;
    //     }

    //     const commitmentEval = this.polyUtils.commitEvaluation(accValue);
    //     const isValid = this.validateEvalCommit(commitmentEval, this.myAccount);

    //     return isValid;
    // }

    private checkPolyShared(sender: string): void {
        const countEncShares = this.rngStorage.countEncSharesMessage();
        this.logger.log({subject: "Info", message: `sender ${sender}, countEncShares ${countEncShares}`});
        if (countEncShares === this.rngStorage.getWhiteListSize()) {
            this.implicateTrigger.registerOnTrigger(() => this.moveToNextRound());

            const encShares = this.rngStorage.getEncShares(this.myAccount);
            const accValue = this.polyUtils!.assembleValue(encShares);

            if (accValue == undefined) {
                this.implicateFaultyParty();
                return;
            }

            const commitmentEval = this.polyUtils!.commitEvaluation(accValue);

            const isValid = this.validateEvalCommit(commitmentEval, this.myAccount);
            if (isValid) {
                const contentEvalCommit = this.messagesFactory!.createEvalCommitMessage(commitmentEval);
                const contentPolyEval = this.messagesFactory!.createPolyEvalMessage(accValue);

                this.sendEvalCommit(contentEvalCommit);

                this.rngStorage.storePolyEvalMessage(this.myAccount, contentPolyEval);
                this.rngStorage.storeEvalCommitMessage(this.myAccount, contentEvalCommit);
                this.checkEvalCommited();
            } else {
                this.implicateFaultyParty();
            }
        }
    }

    private implicateFaultyParty(): void {
        this.logger.log({subject: "Warning", message: `Implicating faulty party...`});
        if (this.keyManager) {
            const privateKey = this.keyManager.getMyPrivateKey();

            // const publicKey = this.keyManager.getMyPublicKey();
            // console.log(this.myAccount, publicKey, privateKey);

            const content = this.messagesFactory!.createImplicateMessage(privateKey);
            this.sendImplicate(content);

            this.validateEncShares(this.myAccount, privateKey);
        }
    }

    private sendImplicate(content: IImplicateMessageContent): void {
        const consesnsusRawMessage: IProtocolRawMessage = {
            messageType: MessageType.IMPLICATE,
            view: this.view,
            content: serializeMessageContent(content)
        };

        this.networkCommunication.sendMessage(this.topicId, consesnsusRawMessage);
    }

    private validateEncShares(victimAccount: string, victimPrivateKey: string): void {
        const victimPublicKey = this.rngStorage.getPk(victimAccount);
        if (victimPublicKey === undefined && this.polyUtils === undefined) {
            return undefined;
        }

        const victimKeyManager = this.keyManagerFactory.createWithKeyPair(victimPrivateKey, victimPublicKey!);

        const encShares = this.rngStorage.getEncShares(victimAccount);
        const commitment = this.rngStorage.getPolyCommitment(victimAccount);

        for (let i = 0; i < encShares.length; i++) {
            let share: Buffer;
            try {
                share = victimKeyManager.decrypt(encShares[i]);
            } catch (e) {
                this.blackList.add(i);
                continue;
            }

            const isValidShare = this.polyUtils!.verifyEval(toBigIntLE(share), commitment[i]);
            if (!isValidShare) {
                this.blackList.add(i);
            }
        }
    }

    public onReceiveImplicate(content: IImplicateMessageContent, sender: string): void {
        // TODO: check privateKey and publicKey

        this.validateEncShares(sender, content.privateKey);
        // console.log(this.blackList);
    }

    private sendEvalCommit(content: IEvalCommitMessageContent): void {
        const consesnsusRawMessage: IProtocolRawMessage = {
            messageType: MessageType.EVAL_COMMIT,
            view: this.view,
            content: serializeMessageContent(content)
        };

        this.networkCommunication.sendMessage(this.topicId, consesnsusRawMessage);
        this.logger.log({
            subject: "ConsensusService",
            message: "eval_commit"
        });
    }

    public onReceiveEvalCommit(message: IEvalCommitMessageContent, sender: string): any {
        const isValid = this.validateEvalCommit(message.commitment, sender);
        if (isValid) {
            this.rngStorage.storeEvalCommitMessage(sender, message);
            this.checkEvalCommited();
        }
    }

    private checkEvalCommited(): void {
        const countEvalCommitMessages = this.rngStorage.countEvalCommitMessages();
        this.logger.log({subject: "Info", message: `countEvalCommitMessages ${countEvalCommitMessages}`});
        if (countEvalCommitMessages === this.rngStorage.getWhiteListSize()) {
            const evalMessageContent = this.rngStorage.getPolyEvalMessage(this.myAccount);
            if (evalMessageContent) {
                this.sendPolyEval(evalMessageContent);

                // this.rngStorage.storePolyEvalMessage(this.myAccount, evalMessageContent);
                // this.checkPolyEval();
            }
        }
    }

    private sendPolyEval(content: IPolyEvalMessageContent): void {
        const consesnsusRawMessage: IProtocolRawMessage = {
            messageType: MessageType.POLY_EVAL,
            view: this.view,
            content: serializeMessageContent(content)
        };

        this.networkCommunication.sendMessage(this.topicId, consesnsusRawMessage);
        this.logger.log({
            subject: "ConsensusService",
            message: "poly_eval"
        });
    }

    public onReceivePolyEval(message: IPolyEvalMessageContent, sender: string): void {
        // TODO: handle undefined
        const commitmentEval = this.rngStorage.getEvalCommitMessage(sender)!.commitment;
        const isValid = this.polyUtils!.verifyEval(toBigIntLE(message.evaluation), commitmentEval);
        if (isValid) {
            this.rngStorage.storePolyEvalMessage(sender, message);
            this.checkPolyEval();
        }
    }

    private checkPolyEval(): void {
        const countPolyEvalMessages = this.rngStorage.countPolyEvalMessages();
        const quorumSize = this.rngStorage.getQuorumSize();
        // const quorumSize = this.rngStorage.getWhiteListSize();
        if (countPolyEvalMessages === quorumSize) {
            const evals = this.rngStorage.getPolyEvaluations();
            if (evals.length < quorumSize) {
                // TODO: handle this error
                return;
            }
            const randomNumber = this.polyUtils!.interpolatePolynomial(evals.slice(0, quorumSize), this.fieldValue);
            // this.roundTrigger.unregisterOnTrigger();
            this.onGeneratedNumber(randomNumber);
            // console.log(this.myAccount, randomNumber);
        }
    }
}
