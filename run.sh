#!/bin/bash
for run in {1..3}
do
    for (( i = 3; i <= 10; i++ ))
    do
        PARTIES=$i node --experimental-worker dist/test/performance/Performance.test.js
    done
done