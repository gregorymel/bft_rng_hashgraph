import { Node } from "./Node";
import { ConsensusServiceMock } from "../consensusService/ConsensusServiceMock";
import { ConsensusServiceDiscovery } from "../consensusService/ConsensusServiceDiscovery";

// import { Block } from "../../src";

export class TestNetwork {
    public nodes: Node[] = [];

    constructor(public discovery?: ConsensusServiceDiscovery) {
    }

    // async provideNextBlock(): Promise<any> {
    //     return Promise.all(this.nodes.map(n => n.blockUtils.provideNextBlock()));
    // }

    // async resolveAllValidations(isValid: boolean, excludedNodes: Node[] = []): Promise<any> {
    //     const relevantNodes: Node[] = this.nodes.filter(n => excludedNodes.indexOf(n) === -1);
    //     const blockUtilsList: BlockUtilsMock[] = relevantNodes.map(n => n.blockUtils);
    //     const promises = blockUtilsList.map(bu => bu.resolveAllValidations(isValid));
    //     return Promise.all(promises);
    // }

    getNetworkSize(): number {
        return this.nodes.length;
    }

    getNodeService(accountId: string): ConsensusServiceMock | undefined {
        if (this.discovery) {
            return this.discovery.getServiceByAccount(accountId);
        }

        return undefined;
    }

    getAllTopics(): Array<string> {
        if (this.discovery) {
            return this.discovery.getAllTopics();
        }

        return [];
    }

    registerNode(node: Node): void {
        this.nodes.push(node);
    }

    registerNodes(nodes: Node[]): void {
        nodes.forEach(node => this.registerNode(node));
    }

    initGenerationOnAllNodes(fieldValue: bigint): void {
        for (const node of this.nodes) {
            node.initGeneration(fieldValue);
        }
    }

    startGenerationOnAllNodes(): void {
        for (const node of this.nodes) {
            node.startGeneration();
        }
    }

    shutDown(): void {
        this.nodes.forEach(node => node.dispose());
    }
}