import { IConfig  } from "../../src/Config";
import { BftRNG } from "../../src/RNG";
import { INetworkCommunication } from "../../src/networkCommunication/NetworkCommunication";
// import { ConsensusServiceMock } from "../consensusService/ConsensusServiceMock";
import { InMemoryStorage } from "../../src/storage/RNGStorage";
import { KeyManagerFactory, KeyManagerRSA } from "../../src/keyManager/KeyManager";
import { PolyUtilsP256, PolyUtilsCtor } from "../../src/polynomialUtils/PolynomialUtils";
import { Logger } from "../../src/logger/Logger";
import { TriggerMock } from "../roundTrigger/RoundTriggerMock";

export class Node {
    public keyManagerFactory: KeyManagerFactory;
    public rngStorage: InMemoryStorage;
    public polyUtilsCtor: PolyUtilsCtor;
    public consensusService: INetworkCommunication;
    private logger: Logger;
    private rng: BftRNG;
    private topicId: string;
    private roundTrigger: TriggerMock;
    private implicateTrigger: TriggerMock;

    constructor(consensusService: INetworkCommunication,
                topicId: string,
                whitelist: string[],
                logger: Logger) {
        this.logger = logger;
        this.consensusService = consensusService;
        this.rngStorage = new InMemoryStorage(whitelist, this.logger);
        this.keyManagerFactory = new KeyManagerFactory(KeyManagerRSA);
        this.polyUtilsCtor = PolyUtilsP256;
        this.topicId = topicId;
        this.roundTrigger = new TriggerMock();
        this.implicateTrigger = new TriggerMock();
        this.rng = new BftRNG(this.buildConfig());
    }

    public triggerNewRound(): void {
        this.roundTrigger.trigger();
    }

    public triggerImplicate(): void {
        this.implicateTrigger.trigger();
    }

    get operatorId(): string {
        return this.consensusService.getOperatorAccount();
    }

    public buildConfig(): IConfig {
        return {
            topicId: this.topicId,
            networkCommunication: this.consensusService,
            polyUtilsCtor: this.polyUtilsCtor,
            keyManagerFactory: this.keyManagerFactory,
            rngStorage: this.rngStorage,
            logger: this.logger,
            implicateTrigger: this.implicateTrigger,
            roundTrigger: this.roundTrigger
        };
    }

    public registerOnNewNumber(cb: (value: bigint) => void): void {
        this.rng.registerOnCommitted(value => cb(value));
        // this.blockChain.appendBlockToChain(block);
    }

    public initGeneration(fieldValue: bigint): void {
        if (this.rng) {
            this.rng.createRNGTerm(fieldValue);
        }
    }

    public startGeneration(): void {
        if (this.rng) {
            this.rng.startRNGTerm();
        }
    }

    public dispose(): void {
        if (this.rng) {
            this.rng.dispose();
        }
    }
}