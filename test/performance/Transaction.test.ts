import { HederaConsensusService } from "../../src/networkCommunication/ConsensusService";
import { MessageType, IProtocolRawMessage } from "../../src/networkCommunication/Messages";

// utils
import { aPolyCommitMessage } from "../builders/MessageBuilder";
import { KeyManagerRSA, KeyManagerFactory } from "../../src/keyManager/KeyManager";
import { PolyUtilsP256 } from "../../src/polynomialUtils/PolynomialUtils";
import { primeSync, randBytesSync } from "bigint-crypto-utils";
import { prepareMesage } from "../consensusService/ConsensusServiceTestUtils";
import { Ed25519PrivateKey } from "@hashgraph/sdk";
import { readdirSync, readFileSync, appendFileSync } from "fs";

async function main() {
    const mirrorNodeAddress = "hcs.testnet.mirrornode.hedera.com:5600";
    const topicId = "0.0.56323";
    const keyFolder = "./config";
    const keyArray: Array<[string, Ed25519PrivateKey]> = new Array();

    readdirSync(keyFolder).forEach((file) => {
        const operatorAccount = file.slice(0, -4);

        const fileContent = readFileSync(keyFolder + "/" + file, "utf-8");
        const operatorPrivateKey = Ed25519PrivateKey.fromString(fileContent);

        keyArray.push([operatorAccount, operatorPrivateKey]);
    });

    const [accountNode0, privateKeyNode0] = keyArray[0];
    const broadcaster = new HederaConsensusService(privateKeyNode0, accountNode0, mirrorNodeAddress);

    const [accountNode1, privateKeyNode1] = keyArray[1];
    const listener1 = new HederaConsensusService(privateKeyNode1, accountNode1, mirrorNodeAddress);
    const onMessagePromise: Promise<[string, IProtocolRawMessage]> = new Promise((resolve) => {
        listener1.registerOnMessage(topicId, (message, sender) => {
            resolve([sender, message]);
        });
    });
    // console.time();

    const degree = 2;
    const fieldValue = primeSync(128);
    const keyManager = (new KeyManagerFactory(KeyManagerRSA)).createNewKeyManager(2048);
    const polyUtils = new PolyUtilsP256(keyManager, 128);
    const commitedPoly = polyUtils.generatePolynomial(degree, fieldValue);

    const PCMessage = prepareMesage(
        MessageType.POLY_COMMIT,
        aPolyCommitMessage(
            polyUtils,
            commitedPoly,
            3,
            keyManager.getMyPublicKey()
        ),
        0
    );

    // console.log(PCMessage);

    const start = process.hrtime();
    await broadcaster.sendMessage(topicId, PCMessage);


    await onMessagePromise;
    const end = process.hrtime(start);
    const time = `${end[0] * 1000 + end[1] / 1000000}`;

    return time;
}

const promises = [...Array(5)].map(() => main());
Promise.all(promises)
    .then(results => {
        const data = results.join(" ") + "\n";
        appendFileSync("tx_time.txt", data);
        process.exit(0);
    });