import { getHederaAccounts } from "../consensusService/ConsensusServiceTestUtils";
import { HederaConsensusService } from "../../src/networkCommunication/ConsensusService";
import { NodeBuilder } from "../builders/NodeBuilder";
import * as cluster from "cluster";
import { primeSync } from "bigint-crypto-utils";
import { appendFileSync } from "fs";

function main(PARTIES_NUM: number) {
    const mirrorNodeAddress = "hcs.testnet.mirrornode.hedera.com:5600";
    const hederaAccounts = getHederaAccounts("./config", PARTIES_NUM);
    const services = hederaAccounts
        .map(account => new HederaConsensusService(account[1], account[0], mirrorNodeAddress));

    // console.log(services.length);

    // services[0].createTopic()
    //     .then(console.log);
    // console.log(topicId);
    const topicId = "0.0.56322";
    const whitelist = hederaAccounts.map(account => account[0]);

    const timeResults: Array<string> = [];
    if (cluster.isMaster) {
        const fieldValue = primeSync(16);
        console.log("main -> fieldValue", fieldValue);
        for (let i = 0; i < PARTIES_NUM; i++) {
            const worker = cluster.fork({
                PROCCESS_IDX: i,
                DEBUG: "*",
                fieldValue: fieldValue.toString()
             });

            worker.on("message", (time) => {
                timeResults.push(time);

                if (timeResults.length === PARTIES_NUM) {
                    const data = timeResults.join(" ") + "\n";
                    appendFileSync("result.txt", data);
                }
            });
        }
    } else {
        const fieldValue = BigInt(process.env["fieldValue"]);
        const i = Number(process.env["PROCCESS_IDX"]);
        let start: [number, number];
        // console.log(`Worker ${process.pid} with id ${i}`);
        // console.log(fieldValue);

        const node = new NodeBuilder()
            .thatIsPartOf(services[i])
            .withTopicId(topicId)
            .withWhiteList(whitelist)
            .build();

        node.registerOnNewNumber((number) => {
            console.log(`Worker ${process.pid} with id ${i} generated number ${number}`);
            const end = process.hrtime(start);
            const time = `${end[0] * 1000 + end[1] / 1000000}`;
            process.send!(time);
            process.exit(0);
        });

        node.initGeneration(fieldValue);
        start = process.hrtime();
        setTimeout(() => {
            node.startGeneration();
        }, 1000);
    }
}

// const parties = 12;
const parties = Number(process.env.PARTIES);
console.log(parties);
main(parties);