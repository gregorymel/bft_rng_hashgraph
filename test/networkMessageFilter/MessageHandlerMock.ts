import { IPolyCommitMessageContent, IPolyShareMessageContent } from "../../src/networkCommunication/Messages";
import { MessagesHandler } from "../../src/networkCommunication/MessagesHandler";

export class MessagesHandlerMock implements MessagesHandler {
    public onReceivePolyCommit(message: IPolyCommitMessageContent): any {
    }

    public onReceivePolyShare(message: IPolyShareMessageContent): any {
    }
}