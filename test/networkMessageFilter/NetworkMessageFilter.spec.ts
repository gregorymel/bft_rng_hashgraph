// BDD
import * as chai from "chai";
import { expect } from "chai";
import * as sinon from "sinon";
import sinonChai from "sinon-chai";

import { aTestNetwork } from "../builders/TestNetworkBuilder";
import { MessagesHandlerMock }  from "../networkMessageFilter/MessageHandlerMock";
import { prepareMesage } from "../consensusService/ConsensusServiceTestUtils";
import { aPolyCommitMessage, aPolyShareMessage } from "../builders/MessageBuilder";

import { Polynomial, PolyUtilsP256 } from "../../src/polynomialUtils/PolynomialUtils";
import { KeyManagerRSA } from "../../src/keyManager/KeyManager";
import { randBetween, toZn } from "bigint-crypto-utils";

// module under test
import { MessageType, IPolyCommitMessageContent } from "../../src/networkCommunication/Messages";
import { NetworkMessagesFilter } from "../../src/networkCommunication/NetworkMessageFilter";
import { MessagesHandler } from "../../src/networkCommunication/MessagesHandler";
import { serializeMessageContent, deserializeMessageContent } from "../../src/networkCommunication/Messages";

chai.use(sinonChai);

describe("Network Message Filter", () => {
    // it.skip("shoud be able to recieve messages from consensus service", async () => {
    //     // a network with 4 nodes
    //     const testNetwork = aTestNetwork();
    //     const nodeUnderTest = testNetwork.nodes[0];
    //     const senderNode = testNetwork.nodes[1];
    //     const senderService = testNetwork.getNodeService(senderNode.publicKey)!;
    //     const topicId = await senderService.createTopic();

    //     const messageFilter: NetworkMessagesFilter = new NetworkMessagesFilter(nodeUnderTest.consensusService, topicId);
    //     const messagesHandler: MessagesHandler = new MessagesHandlerMock();

    //     const PCSpy = sinon.spy(messagesHandler, "onReceivePolyCommit");
    //     const PSSpy = sinon.spy(messagesHandler, "onReceivePolyShare");
    //     // const CSpy = sinon.spy(messagesHandler, "onReceiveCommit");
    //     // const VCSpy = sinon.spy(messagesHandler, "onReceiveViewChange");
    //     // const NVSpy = sinon.spy(messagesHandler, "onReceiveNewView");

    //     messageFilter.setMessageHandler(messagesHandler);

    //     // const block: Block = aBlock(theGenesisBlock);
    //     // const pks = testNetwork.discovery.getAllServicesIds();
    //     senderService.sendMessage(topicId, prepareMesage(MessageType.POLY_COMMIT, aPolyCommitMessage()));
    //     senderService.sendMessage(topicId, prepareMesage(MessageType.POLY_SHARE, aPolyCommitMessage()));
    //     // senderService.sendMessage(topicId, "asd");
    //     // senderGossip.sendMessage(pks, messageToCS(aCommitMessage(senderNode.keyManager, 3, 0, block)));
    //     // senderGossip.sendMessage(pks, messageToCS(aViewChangeMessage(senderNode.keyManager, 3, 0)));
    //     // senderGossip.sendMessage(pks, messageToCS(aNewViewMessage(senderNode.keyManager, 3, 0, aPrePrepareMessage(senderNode.keyManager, 3, 0, block), [])));

    //     expect(PCSpy).to.have.been.calledOnce;
    //     expect(PSSpy).to.have.been.calledOnce;
    //     // expect(CSpy).to.have.been.calledOnce;
    //     // expect(VCSpy).to.have.been.calledOnce;
    //     // expect(NVSpy).to.have.been.calledOnce;
    // });


    describe("message (de)serialization", () => {
        let keyManager: KeyManagerRSA;
        let polyUtils: PolyUtilsP256;
        let poly: Polynomial;
        let myPk: string;

        before(() => {
            keyManager = new KeyManagerRSA();
            keyManager.generateNewKeys(2048);

            polyUtils = new PolyUtilsP256(keyManager);
            const degree = 2;
            // const degree = Math.floor(Math.random() * 100);
            // 128 bit - random number
            const filedValue = randBetween(BigInt("0xffffffffffffffffffffffffffffffff"));
            poly = polyUtils.generatePolynomial(degree, filedValue, 128);
            myPk = keyManager.getMyPublicKey()!;
        });

        it("should (de)serialize PolyCommit message", () => {
            const expectedMsg = aPolyCommitMessage(polyUtils, poly, myPk);
            const contentStr = serializeMessageContent(expectedMsg);
            console.log(contentStr);

            const actualMsg = deserializeMessageContent(contentStr) as IPolyCommitMessageContent;

            expect(actualMsg).to.deep.equal(expectedMsg);
        });
    });
});