// BDD
import * as chai from "chai";
import { expect } from "chai";
import * as sinon from "sinon";
import sinonChai from "sinon-chai";

// module under test
import { KeyManagerRSA } from "../../src/keyManager/KeyManager";

chai.use(sinonChai);

describe("KeyManagerRSA", () => {
    it("should encrypt data to the given public key", () => {
        const keyManager1 = new KeyManagerRSA();
        const keyManager2 = new KeyManagerRSA();

        keyManager1.generateNewKeys(2048);
        keyManager2.generateNewKeys(2048);
        const publicKey1 = keyManager1.getMyPublicKey();
        const publicKey2 = keyManager2.getMyPublicKey();

        const expectedData = Buffer.from("TEST_MESSAGE", "utf-8");
        const ciphertext = keyManager1.encrypt(expectedData, publicKey2!);
        const actualData = keyManager2.decrypt(ciphertext);

        expect(actualData).to.deep.equal(expectedData);
    });
});