import { Trigger } from "../../src/roundTrigger/RoundTrigger";

export class TriggerMock implements Trigger {
    private cb?: () => void;

    public registerOnTrigger(cb: () => void): void {
        this.cb = cb;
    }

    public unregisterOnTrigger(): void {
        this.cb = undefined;
    }

    public trigger(): void {
        if (this.cb) {
            this.cb();
        }
    }
}