// BDD
import * as chai from "chai";
import { expect } from "chai";
import * as sinon from "sinon";
import sinonChai from "sinon-chai";
chai.use(sinonChai);

import { aTestNetwork } from "./builders/TestNetworkBuilder";
import { TestNetwork } from "./network/TestNetwork";
import { Node } from "./network/Node";
import { aPolyCommitMessage, aPolyShareMessage } from "./builders/MessageBuilder";
import { messageCounter, prepareMesage } from "./consensusService/ConsensusServiceTestUtils";
import { IConfig } from "../src/Config";
import { IKeyManager } from "../src/keyManager/KeyManager";
import { IPolyUtils, Polynomial } from "../src/polynomialUtils/PolynomialUtils";

import { toZn, primeSync } from "bigint-crypto-utils";

// module under test
import { BftRNGTerm } from "../src/RNGTerm";
import { MessageType } from "../src/networkCommunication/Messages";

describe("RNGTerm", () => {
    let testNetwork: TestNetwork;
    let node0: Node;
    let node1: Node;
    let node2: Node;
    let node3: Node;
    let fieldValue: bigint;

    before(() => {
        fieldValue = primeSync(128);
    });

    describe.skip("normal flow of the algorithm", () => {
        beforeEach(() => {
            testNetwork = aTestNetwork();

            testNetwork.initGenerationOnAllNodes(fieldValue);

            node0 = testNetwork.nodes[0];
            node1 = testNetwork.nodes[1];
            node2 = testNetwork.nodes[2];
            node3 = testNetwork.nodes[3];
        });

        afterEach(() => {
            testNetwork.shutDown();
        });

        it.skip("should gather the same whitelised pks after POLY_COMMIT messages", () => {
            testNetwork.startGenerationOnAllNodes();

            const node0Pks = node0.rngStorage.getWhiteListedPks();
            const node1Pks = node1.rngStorage.getWhiteListedPks();
            const node2Pks = node2.rngStorage.getWhiteListedPks();
            const node3Pks = node3.rngStorage.getWhiteListedPks();

            expect(node0Pks).to.deep.equal(node1Pks);
            expect(node2Pks).to.deep.equal(node3Pks);
            expect(node0Pks).to.deep.equal(node2Pks);
            expect(node0Pks).to.not.include(undefined);
        });

        it.skip("should sendEvalCommit if check is passed", () => {
            const spy = sinon.spy(node0.consensusService, "sendMessage");

            testNetwork.startGenerationOnAllNodes();

            node3.rngStorage.getWhiteListSize();
            expect(messageCounter(spy, MessageType.EVAL_COMMIT)).to.equal(1);
        });

        it("should sendPolyEval if check is passed", () => {
            const node0Spy = sinon.spy(node0.consensusService, "sendMessage");
            const node1Spy = sinon.spy(node1.consensusService, "sendMessage");
            const node2Spy = sinon.spy(node2.consensusService, "sendMessage");
            const node3Spy = sinon.spy(node3.consensusService, "sendMessage");

            testNetwork.startGenerationOnAllNodes();

            node3.rngStorage.getWhiteListSize();
            expect(messageCounter(node0Spy, MessageType.POLY_EVAL)).to.equal(1);
            expect(messageCounter(node1Spy, MessageType.POLY_EVAL)).to.equal(1);
            expect(messageCounter(node2Spy, MessageType.POLY_EVAL)).to.equal(1);
            expect(messageCounter(node3Spy, MessageType.POLY_EVAL)).to.equal(1);
        });

        it("all nodes should agree on the same number", () => {
            // TODO: implement this case
        });
    });

    describe("flow of the algorithm with byzantine nodes", () => {
        let byzantineNode: Node;
        let degree: number;

        beforeEach(() => {
            testNetwork = aTestNetwork(4);
            degree = 2;

            testNetwork.initGenerationOnAllNodes(fieldValue);

            node0 = testNetwork.nodes[0];
            node1 = testNetwork.nodes[1];
            node2 = testNetwork.nodes[2];
            // node3 = testNetwork.nodes[3];
            byzantineNode = testNetwork.nodes[3];
        });

        it("should reach consensus, in a network of 4 nodes, where byzantine node send invalid PolyShare to one node", () => {
            const node0Spy = sinon.spy(node0.consensusService, "sendMessage");

            const topicId = testNetwork.getAllTopics()[0];
            const service = testNetwork.getNodeService(byzantineNode.operatorId)!;

            node0.startGeneration();
            node1.startGeneration();
            node2.startGeneration();

            const keyManager = byzantineNode.keyManagerFactory.createNewKeyManager(2048);
            const polyUtils = new byzantineNode.polyUtilsCtor(keyManager, 128);
            const commitedPoly = polyUtils.generatePolynomial(degree, fieldValue);
            const fakePoly = polyUtils.generatePolynomial(degree, fieldValue);

            const PCMessage = prepareMesage(
                MessageType.POLY_COMMIT,
                aPolyCommitMessage(
                    polyUtils,
                    commitedPoly,
                    testNetwork.getNetworkSize(),
                    keyManager.getMyPublicKey()
                ),
                0
            );

            service.sendMessage(topicId, PCMessage);

            const pks = node0.rngStorage.getWhiteListedPks();

            const PSMessageContent = aPolyShareMessage(polyUtils, commitedPoly, pks);
            const PSMessageContentFake = aPolyShareMessage(polyUtils, fakePoly, pks);
            // Send invalid share for Node 0
            PSMessageContent.shares[0] = PSMessageContentFake.shares[0];
            const PSMessage = prepareMesage(MessageType.POLY_SHARE, PSMessageContent, 0);

            service.sendMessage(topicId, PSMessage);
            expect(messageCounter(node0Spy, MessageType.IMPLICATE)).to.equal(1);

            node0.triggerImplicate();
            node1.triggerImplicate();
            node2.triggerImplicate();
        });

        it("should reach consensus, in a network of 4 nodes, where byzantine node send invalid PolyShare to many nodes", () => {
            const node0Spy = sinon.spy(node0.consensusService, "sendMessage");
            const node1Spy = sinon.spy(node1.consensusService, "sendMessage");
            const node2Spy = sinon.spy(node2.consensusService, "sendMessage");

            const topicId = testNetwork.getAllTopics()[0];
            const service = testNetwork.getNodeService(byzantineNode.operatorId)!;

            node0.startGeneration();
            node1.startGeneration();
            node2.startGeneration();

            const keyManager = byzantineNode.keyManagerFactory.createNewKeyManager(2048);
            const polyUtils = new byzantineNode.polyUtilsCtor(keyManager, 128);
            const commitedPoly = polyUtils.generatePolynomial(degree, fieldValue);
            const fakePoly = polyUtils.generatePolynomial(degree, fieldValue);

            const PCMessage = prepareMesage(
                MessageType.POLY_COMMIT,
                aPolyCommitMessage(
                    polyUtils,
                    commitedPoly,
                    testNetwork.getNetworkSize(),
                    keyManager.getMyPublicKey()
                ),
                0
            );

            service.sendMessage(topicId, PCMessage);

            const pks = node0.rngStorage.getWhiteListedPks();

            // const PSMessageContent = aPolyShareMessage(polyUtils, commitedPoly, pks);
            // PSMessageContent.shares[0] = PSMessageContentFake.shares[0];
            const PSMessageContentFake = aPolyShareMessage(polyUtils, fakePoly, pks);
            // Send invalid share to all nodes
            const PSMessage = prepareMesage(MessageType.POLY_SHARE, PSMessageContentFake, 0);

            service.sendMessage(topicId, PSMessage);
            expect(messageCounter(node0Spy, MessageType.IMPLICATE)).to.equal(1);
            expect(messageCounter(node1Spy, MessageType.IMPLICATE)).to.equal(1);
            expect(messageCounter(node2Spy, MessageType.IMPLICATE)).to.equal(1);

            node0.triggerImplicate();
            node1.triggerImplicate();
            node2.triggerImplicate();
        });
    });
});
