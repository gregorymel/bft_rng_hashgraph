import { Block } from "../../src/Block";
import { Logger, ConsoleLogger } from "../../src/logger/Logger";
// import { BlockUtilsMock } from "../blockUtils/BlockUtilsMock";
// import { Gossip } from "../gossip/Gossip";
// import { ConsoleLogger } from "../logger/ConsoleLogger";
// import { SilentLogger } from "../logger/SilentLogger";
import { Node } from "../network/Node";
import { INetworkCommunication } from "../../src/networkCommunication/NetworkCommunication";
// import { ConsensusServiceMock } from "../consensusService/ConsensusServiceMock";
// import { ConsensusServiceDiscovery } from "../consensusService/ConsensusServiceDiscovery";

export class NodeBuilder {
    private consensusService?: INetworkCommunication = undefined;
    private topicId?: string = undefined;
    private whitelist?: string[];
    private logger?: Logger;
    // private logsToConsole: boolean = false;

    constructor() {}

    public thatIsPartOf(consensusService: INetworkCommunication): this {
        if (!this.consensusService) {
            this.consensusService = consensusService;
        }
        return this;
    }

    public withTopicId(topicId?: string): this {
        this.topicId = topicId;
        return this;
    }

    public withWhiteList(whitelist?: string[]): this {
        this.whitelist = whitelist;
        return this;
    }

    public thatLogsTo(logger: Logger): this {
        if (!this.logger) {
            this.logger = logger;
        }
        return this;
    }

    // public withBlocksPool(blocksPool: Block[]): this {
    //     if (!this.blocksPool) {
    //         this.blocksPool = blocksPool;
    //     }
    //     return this;
    // }

    // public get thatLogsToConsole(): this {
    //     this.logsToConsole = true;
    //     return this;
    // }

    public build(): Node {
        const topicId = this.topicId || "defaultTopic";
        let accountId: string | undefined = undefined;
        if (this.consensusService) {
            accountId = this.consensusService.getOperatorAccount();
        } else {
            accountId = "defaultId";
        }

        const logger: Logger = this.logger ? this.logger :  new ConsoleLogger(accountId);
        const whitelist = this.whitelist || new Array<string>();

        return new Node(this.consensusService!, topicId, whitelist, logger);
    }
}
