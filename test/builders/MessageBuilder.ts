import { MessagesFactory } from "../../src/networkCommunication/MessageFactory";
import { IPolyCommitMessageContent, IPolyShareMessageContent } from "../../src/networkCommunication/Messages";
import { IPolyUtils, Polynomial } from "../../src/polynomialUtils/PolynomialUtils";

export function aPolyCommitMessage(polyUtils: IPolyUtils, poly: Polynomial, partiesNum: number, pk: string): IPolyCommitMessageContent {
    const mf = new MessagesFactory(polyUtils);
    return mf.createPolyCommitMessage(poly, partiesNum, pk);
}

export function aPolyShareMessage(polyUtils: IPolyUtils,  poly: Polynomial, pks: string[]): IPolyShareMessageContent {
    const mf = new MessagesFactory(polyUtils);
    return mf.createPolyShareMessage(poly, pks);
}