// import { Logger } from "../../src/logger/Logger";
import { ConsensusServiceMock } from "../consensusService/ConsensusServiceMock";
import { ConsensusServiceDiscovery } from "../consensusService/ConsensusServiceDiscovery";
import { Logger, ConsoleLogger } from "../../src/logger/Logger";
// import { SilentLogger } from "../logger/SilentLogger";
import { Node } from "../network/Node";
import { TestNetwork } from "../network/TestNetwork";
// import { aBlock, theGenesisBlock } from "./BlockBuilder";
import { NodeBuilder } from "./NodeBuilder";

export interface LoggerConstructor {
    new(id: string): Logger;
}

export class With {
    constructor(private testNetworkBuilder: TestNetworkBuilder, private count: number) {

    }
    get nodes() {
        this.testNetworkBuilder.countOfNodes = this.count;
        return this.testNetworkBuilder;
    }
}

export class TestNetworkBuilder {
    private loggerCtor: LoggerConstructor = ConsoleLogger;
    private customNodes: Node[] = [];
    // private blocksPool: Block[];
    private topicCreated: boolean = true;

    public countOfNodes: number = 0;

    public thatLogsToConsole(): this {
        this.thatLogsToCustomeLogger(ConsoleLogger);
        return this;
    }

    public thatLogsToCustomeLogger(ctor: LoggerConstructor): this {
        this.loggerCtor = ctor;
        return this;
    }

    public with(count: number) {
        return new With(this, count);
    }

    public withCustomNodes(customNodes: Node[]): this {
        this.customNodes.push(...customNodes);
        return this;
    }

    // public withTopicCreated(): this {
    //     this.topicCreated = true;
    //     return this;
    // }

    public build(): TestNetwork {
        // const blocksPool: Block[] = this.buildBlocksPool();
        const discovery = new ConsensusServiceDiscovery();
        const topicId = discovery.createTopic();
        const nodes = this.createNodes(discovery, topicId);
        const testNetwork = new TestNetwork(discovery);
        testNetwork.registerNodes(nodes);
        return testNetwork;
    }

    private buildNode(builder: NodeBuilder, accountId: string, discovery: ConsensusServiceDiscovery,
                      whitelist?: string[], topicId?: string): Node {
        const service = new ConsensusServiceMock(accountId, discovery);
        const logger: Logger = new this.loggerCtor(accountId);
        discovery.registerService(accountId, service);
        return builder
            .thatIsPartOf(service)
            .thatLogsTo(logger)
            .withTopicId(topicId)
            .withWhiteList(whitelist)
            .build();
    }

    private createNodes(discovery: ConsensusServiceDiscovery, topicId?: string): Node[] {
        const nodes: Node[] = [];

        const whitelist = [...Array(this.countOfNodes).keys()].map(value => `Node${value}`);
        for (let i = 0; i < this.countOfNodes; i++) {
            const nodeBuilder = new NodeBuilder();
            const node = this.buildNode(nodeBuilder, `Node${i}`, discovery, whitelist, topicId);
            nodes.push(node);
        }

        // const customNodes = this.customNodes.map((nodeBuilder, idx) => this.buildNode(nodeBuilder, `Custom-Node ${idx}`, discovery));
        nodes.push(...this.customNodes);
        return nodes;
    }
}

export const aTestNetwork = (countOfNodes: number = 4, topicId?: string) => {
    return new TestNetworkBuilder()
        // .withBlocksPool(blocksPool)
        .with(countOfNodes).nodes
        .thatLogsToConsole()
        // .withTopicId(topicId)
        .build();
};