// BDD
import * as chai from "chai";
import { expect } from "chai";
import * as sinon from "sinon";
import sinonChai from "sinon-chai";
chai.use(sinonChai);


import { TestNetworkBuilder } from "./builders/TestNetworkBuilder";
import { NodeBuilder } from "./builders/NodeBuilder";
import { TestNetwork } from "./network/TestNetwork";
import { HederaConsensusService } from "../src/networkCommunication/ConsensusService";
import { getHederaAccounts } from "./consensusService/ConsensusServiceTestUtils";
import { primeSync } from "bigint-crypto-utils";
import { Node } from "./network/Node";

describe("RNGTerm", () => {
    let testNetwork: TestNetwork;
    let fieldValue: bigint;
    let node0: Node;
    let node1: Node;
    let node2: Node;
    let node3: Node;
    let onNewNumberPromises: Array<Promise<bigint>>;

    before(async () => {
        const mirrorNodeAddress = "hcs.testnet.mirrornode.hedera.com:5600";
        const hederaAccounts = getHederaAccounts("./config");
        const services = hederaAccounts
            .map(account => new HederaConsensusService(account[1], account[0], mirrorNodeAddress));

        // const topicId = await services[0].createTopic();
        // console.log(topicId);
        const topicId = "0.0.27291";
        const whitelist = hederaAccounts.map(account => account[0]);

        const nodes = services.map(service => {
            return new NodeBuilder()
                .thatIsPartOf(service)
                .withTopicId(topicId)
                .withWhiteList(whitelist)
                .build();
        });

        onNewNumberPromises = nodes.map(node => {
            return new Promise(resolve => {
                node.registerOnNewNumber(resolve);
            });
        });

        testNetwork = new TestNetworkBuilder()
            .withCustomNodes(nodes)
            .build();

        node0 = testNetwork.nodes[0];
        node1 = testNetwork.nodes[1];
        node2 = testNetwork.nodes[2];
        node3 = testNetwork.nodes[3];

        fieldValue = primeSync(128);
    });

    after(() => {
        testNetwork.shutDown();
    });

    it("asd", async () => {
        testNetwork.initGenerationOnAllNodes(fieldValue);
        testNetwork.startGenerationOnAllNodes();

        const results = await Promise.all(onNewNumberPromises);

        const node0Pks = node0.rngStorage.getWhiteListedPks();
        // console.log(node0Pks);
        const node1Pks = node1.rngStorage.getWhiteListedPks();
        const node2Pks = node2.rngStorage.getWhiteListedPks();
        const node3Pks = node3.rngStorage.getWhiteListedPks();

        console.log(results);

        expect(node0Pks).to.deep.equal(node1Pks);
        expect(node2Pks).to.deep.equal(node3Pks);
        expect(node0Pks).to.deep.equal(node2Pks);
        expect(node0Pks).to.not.include(undefined);
    });

    // it.skip("should sendEvalCommit if check is passed", () => {
    //     const spy = sinon.spy(node0.consensusService, "sendMessage");

    //     testNetwork.startGenerationOnAllNodes();

    //     node3.rngStorage.getWhiteListSize();
    //     expect(messageCounter(spy, MessageType.EVAL_COMMIT)).to.equal(1);
    // });
});