import {
    INetworkCommunication,
    NetworkCommunicationCallback,
} from "../../src/networkCommunication/NetworkCommunication";
import { IProtocolRawMessage } from "../../src/networkCommunication/Messages";
import { ConsensusServiceDiscovery } from "./ConsensusServiceDiscovery";

export class ConsensusServiceMock implements INetworkCommunication {
    private totalTopics: number = 0;

    constructor(private accountId: string, private discovery: ConsensusServiceDiscovery) {
    }

    public createTopic(): string {
        return this.discovery.createTopic();
    }

    public getOperatorAccount(): string {
        return this.accountId;
    }

    public async sendMessage(topicId: string, rawMessage: IProtocolRawMessage): Promise<void> {
        const subscriptionsList = this.discovery.getTopicSubsriptionList(topicId);
        if (subscriptionsList) {
            subscriptionsList.forEach(cb => {
                cb(rawMessage, this.accountId);
            });
        }
    }

    public registerOnMessage(topicId: string, cb: NetworkCommunicationCallback): void {
        this.discovery.registerOnMessage(topicId, cb);
    }

    public unRegisterOnMessage(topicId: string): void {
        return;
    }
}
