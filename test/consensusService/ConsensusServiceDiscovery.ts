import { ConsensusServiceMock } from "./ConsensusServiceMock";
import {
    INetworkCommunication,
    NetworkCommunicationCallback,
} from "../../src/networkCommunication/NetworkCommunication";

export class ConsensusServiceDiscovery {
    private topicMap: Map<string, NetworkCommunicationCallback[]> = new Map();
    private serviceMap: Map<string, ConsensusServiceMock> = new Map();
    private totalTopics = 0;

    createTopic(): string {
        this.totalTopics++;
        const topicId = `0.0.0.${this.totalTopics}`;
        this.topicMap.set(topicId, new Array());

        return topicId;
    }

    getAllTopics(): Array<string> {
        return [...this.topicMap.keys()];
    }

    registerOnMessage(topicId: string, cb: NetworkCommunicationCallback): void {
        if (this.topicMap.has(topicId)) {
            this.topicMap.get(topicId)!.push(cb);
        }

        return;
    }

    getTopicSubsriptionList(topicId: string): NetworkCommunicationCallback[] | undefined {
        return this.topicMap.get(topicId);
    }

    getServiceByAccount(accountId: string): ConsensusServiceMock | undefined {
        return this.serviceMap.get(accountId);
    }

    registerService(accountId: string, service: ConsensusServiceMock): void {
        this.serviceMap.set(accountId, service);
    }

    // getGossips(pks?: string[]): Gossip[] {
    //     if (pks !== undefined) {
    //         return this.getAllGossipsPks().filter(pk => pks.indexOf(pk) > -1).map(pk => this.getGossipByPk(pk));
    //     } else {
    //         return Array.from(this.serviceMap.values());
    //     }
    // }

    getAllServicesIds(): string[] {
        return Array.from(this.serviceMap.keys());
    }
}
