import {
    MessageType,
    IMessageContent,
    IProtocolRawMessage,
    serializeMessageContent
} from "../../src/networkCommunication/Messages";
import { readdirSync, readFileSync } from "fs";
import { Ed25519PrivateKey } from "@hashgraph/sdk";

export function prepareMesage(type: MessageType, content: IMessageContent, view: number): IProtocolRawMessage {
    return {
        messageType: type,
        view: view,
        content: serializeMessageContent(content)
    };
}

export const messageCounter = (spy: sinon.SinonSpy, messageType: MessageType) => {
    return spy.getCalls()
        .map(c => c.args[1])
        .filter(c => c.messageType === messageType).length;
};

export function getHederaAccounts(pathToFolder: string, num?: number): Array<[string, Ed25519PrivateKey]> {
    const keyArray: Array<[string, Ed25519PrivateKey]> = new Array();

    readdirSync(pathToFolder).forEach((file) => {
        const operatorAccount = file.slice(0, -4);

        const fileContent = readFileSync(pathToFolder + "/" + file, "utf-8");
        const operatorPrivateKey = Ed25519PrivateKey.fromString(fileContent);

        keyArray.push([operatorAccount, operatorPrivateKey]);
    });

    if (num && num <= keyArray.length) {
        return keyArray.slice(0, num);
    }

    return keyArray;
}