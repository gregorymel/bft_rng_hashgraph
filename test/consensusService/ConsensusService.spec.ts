// BDD
import * as chai from "chai";
import { expect } from "chai";
import * as sinon from "sinon";
import sinonChai from "sinon-chai";
chai.use(sinonChai);

// module under test
import { HederaConsensusService } from "../../src/networkCommunication/ConsensusService";
import { MessageType, IProtocolRawMessage } from "../../src/networkCommunication/Messages";

// utils
import { Ed25519PrivateKey } from "@hashgraph/sdk";
import { readdirSync, readFileSync } from "fs";
import { aPolyCommitMessage } from "../builders/MessageBuilder";
import { KeyManagerRSA, KeyManagerFactory } from "../../src/keyManager/KeyManager";
import { PolyUtilsP256 } from "../../src/polynomialUtils/PolynomialUtils";
import { primeSync, randBytesSync } from "bigint-crypto-utils";
import { prepareMesage } from "./ConsensusServiceTestUtils";


describe("ConsensusService", () => {
    const keyArray: Array<[string, Ed25519PrivateKey]> = new Array();

    const mirrorNodeAddress = "hcs.testnet.mirrornode.hedera.com:5600";
    const keyFolder = "./config";
    let dummyMessage: IProtocolRawMessage;
    let topicId: string;

    before(() => {
        readdirSync(keyFolder).forEach((file) => {
            const operatorAccount = file.slice(0, -4);

            const fileContent = readFileSync(keyFolder + "/" + file, "utf-8");
            const operatorPrivateKey = Ed25519PrivateKey.fromString(fileContent);

            keyArray.push([operatorAccount, operatorPrivateKey]);
        });

        // const polyUtils = new PolyUtilsP256();
        // const degree = 3;
        // const fieldValue = primeSync(128);
        // const partiesNum = 4;
        // const poly = polyUtils.generatePolynomial(degree, fieldValue);

        topicId = "0.0.38837";

        dummyMessage = {
            messageType: MessageType.POLY_SHARE,
            view: 0,
            content: randBytesSync(8000).toString("base64")
        };
    });

    it("should be able to broadcast a message to subsribed clients", async () => {
        const [accountNode0, privateKeyNode0] = keyArray[0];
        const broadcaster = new HederaConsensusService(privateKeyNode0, accountNode0, mirrorNodeAddress);

        const [accountNode1, privateKeyNode1] = keyArray[1];
        const listener1 = new HederaConsensusService(privateKeyNode1, accountNode1, mirrorNodeAddress);
        const onMessagePromise: Promise<[string, IProtocolRawMessage]> = new Promise((resolve) => {
            listener1.registerOnMessage(topicId, (message, sender) => {
                resolve([sender, message]);
            });
        });
        console.time();
        await broadcaster.sendMessage(topicId, dummyMessage);


        const [sender, message] = await onMessagePromise;
        console.timeEnd();
        expect(sender).to.equal(accountNode0);
        expect(message).to.deep.equal(dummyMessage);
    });

    it.skip("should be able to broadcast a POLY_SHARE to subsribed clients", async () => {
        const [accountNode0, privateKeyNode0] = keyArray[0];
        const broadcaster = new HederaConsensusService(privateKeyNode0, accountNode0, mirrorNodeAddress);

        const [accountNode1, privateKeyNode1] = keyArray[1];
        const listener1 = new HederaConsensusService(privateKeyNode1, accountNode1, mirrorNodeAddress);
        const onMessagePromise: Promise<[string, IProtocolRawMessage]> = new Promise((resolve) => {
            listener1.registerOnMessage(topicId, (message, sender) => {
                resolve([sender, message]);
            });
        });
        console.time();

        const degree = 51;
        const fieldValue = primeSync(128);
        const keyManager = (new KeyManagerFactory(KeyManagerRSA)).createNewKeyManager(2048);
        const polyUtils = new PolyUtilsP256(keyManager, 128);
        const commitedPoly = polyUtils.generatePolynomial(degree, fieldValue);

        const PCMessage = prepareMesage(
            MessageType.POLY_COMMIT,
            aPolyCommitMessage(
                polyUtils,
                commitedPoly,
                100,
                keyManager.getMyPublicKey()
            ),
            0
        );

        console.log(PCMessage);

        await broadcaster.sendMessage(topicId, PCMessage);


        const [sender, message] = await onMessagePromise;
        console.timeEnd();
        expect(sender).to.equal(accountNode0);
        expect(message).to.deep.equal(PCMessage);
    });
});
