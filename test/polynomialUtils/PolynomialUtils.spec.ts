// BDD
import * as chai from "chai";
import { expect } from "chai";
import * as sinon from "sinon";
import sinonChai from "sinon-chai";

import { randBetween, toZn, primeSync } from "bigint-crypto-utils";
import { toBufferLE, toBigIntLE } from "bigint-buffer";


// module under test
import { Polynomial, PolyUtilsP256 } from "../../src/polynomialUtils/PolynomialUtils";
import { KeyManagerRSA } from "../../src/keyManager/KeyManager";
import { utils } from "mocha";

chai.use(sinonChai);

describe("Polynomial Utils for curve spec256k1", () => {
    let degree: number;
    let filedValue: bigint;
    const bitLength = 128;

    function shuffle<T>(a: Array<T>): Array<T> {
        for (let i = a.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [a[i], a[j]] = [a[j], a[i]];
        }
        return a;
    }

    function buildNetwork(parties: number): [Array<PolyUtilsP256>, Array<string>] {
        const arrParties: PolyUtilsP256[] = new Array();
        const pks: string[] = new Array();
        for (let i = 0; i < parties; i++) {
            const keyManager = new KeyManagerRSA();
            const polyUtils = new PolyUtilsP256(keyManager);

            keyManager.generateNewKeys(2048);

            arrParties.push(polyUtils);
            pks.push(keyManager.getMyPublicKey()!);
        }

        return [arrParties, pks];
    }

    beforeEach(() => {
        degree = Math.floor(Math.random() * 100);
        // degree = 3;
        // 128 bit - prime random number
        // filedValue = randBetween(BigInt("0xffffffffffffffffffffffffffffffff"));
        filedValue = primeSync(bitLength);
        // filedValue  = 19n;
    });

    it.skip("should generate polynomial with given degree and filed value", () => {
        const polyUtils = new PolyUtilsP256();
        const poly = polyUtils.generatePolynomial(degree, filedValue);
        expect(poly.polyCoeffs.length).to.be.equal(degree + 1);
        for (const coeff of poly.polyCoeffs) {
            expect(coeff < filedValue && coeff >= 0n).to.be.true;
        }
    });

    it.skip("should check additive homomorphic property of commitment scheme", () => {
        const polyUtils = new PolyUtilsP256();
        const poly = polyUtils.generatePolynomial(degree, filedValue, bitLength);

        const sum = [...Array(10).keys()]
            .map(i => poly.at(i + 1))
            .reduce((acc, curValue) => acc + curValue);
        const commitment = polyUtils.commitPolynomial(poly, 10);
        const sumCommitment = polyUtils.commitEvaluation(sum);
        const res = polyUtils.verifyEvalCommitment(sumCommitment, commitment);

        expect(res).to.be.true;
    });

    it("should recover value from different subset of points", () => {
        const polyUtils = new PolyUtilsP256();
        // degree = 2;
        const poly = polyUtils.generatePolynomial(degree, filedValue);

        let points = [...Array(2 * degree).keys()]
            .map(i => [i + 1, poly.at(i + 1)]);

        points = shuffle(points);
        // console.log((points as Array<[number, Buffer]>).map((point: [number, Buffer]) => [point[0], toBigIntLE(point[1])]));

        for (let i = 0; i < degree; i++) {
            const slice = points.slice(i, i + degree + 1);
            const res = polyUtils.interpolatePolynomial(slice as Array<[number, bigint]>, filedValue);
            expect(res == poly.at(0)).to.be.true;
        }
    });

    it("should assemble polynimial value from many shares", () => {
        const partyNum = 4;
        const quorumSize = 3;
        const [parties, pks] = buildNetwork(partyNum);

        const polynimials = parties.map((utils) => {
            return utils.generatePolynomial(quorumSize - 1, filedValue, bitLength);
        });

        const shares = polynimials
            .map((poly, i) => parties[i].createEncShares(poly, pks)!);
        const sharesTransposed = shares[0].map((_, i) => shares.map(row => row[i]));

        const resultPoly = polynimials
            .reduce((acc, poly) => acc.add(poly)!); // p_0(x) = \sum p_j(x)

        const commitments = polynimials
            .map((poly, i) => parties[i].commitPolynomial(poly, partyNum));

        const commitmentsTransposed = commitments[0].map((_, i) => commitments.map(row => row[i]));

        const expectedValues = [...Array(partyNum).keys()].map(i => resultPoly.at(i + 1));
        const actualValues = parties.map((utils, i) => utils.assembleValue(sharesTransposed[i])!);
        const actualValuesCommitments = parties.map((utils, i) => utils.commitEvaluation(actualValues[i]));
        const actualValuesZn = actualValues.map(value => toZn(value, filedValue));

        expect(actualValuesZn).to.deep.equal(expectedValues);
        commitmentsTransposed.map((commitment, i) => {
            expect(parties[i].verifyEvalCommitment(actualValuesCommitments[i], commitment)).to.be.true;
        });

        parties.map((utils, i) => {
            const res = utils.verifyEval(actualValues[i], actualValuesCommitments[i]);
            expect(res).to.be.true;
        });

        // const actualPoints: Array<[number, Buffer]> = expectedValues.map((val, i) => [i + 1, toBufferLE(val, bitLength / 8)]);
        // console.log(actualPoints);
        // const resultValue = parties[0].interpolatePolynomial(actualPoints.slice(1, 4), filedValue);
        // console.log(resultValue, resultPoly.at(0));
        // expect(resultValue == resultPoly.at(0)).to.be.true;
    });
});